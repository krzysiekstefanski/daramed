<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

add_filter( 'the_content', 'wpse_280633_break_text' );


get_header();

?>

<section id="kategorie">

	<div class="container">
		<div class="row my-5">
			<div class="col-12">
				<div class="row wow fadeInUp">
					<?php
						// Check if there are any posts to display
						if ( have_posts() ) : ?>

						<?php

						// The Loop
						while ( have_posts() ) : the_post(); ?>
						<div class="col-md-3">
							<div class="row">

								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<div class="post-thumbnail">
								<?php echo $image = get_the_post_thumbnail($post_id, 'custom-image-thumb'); ?>
							</div>
							<div class="meta">
								<?php the_date(); ?>
								<?php the_category(); ?>
							</div>
							<header class="entry-header">
								<?php
								if ( is_single() ) :
									the_title( '<h5 class="entry-title">', '</h5>' );
								else :
									the_title( '<h5 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );
								endif;

								if ( 'post' === get_post_type() ) : ?>
								<!-- <div class="entry-meta">
									<?php wp_bootstrap_starter_posted_on(); ?>
								</div> -->
								<!-- .entry-meta -->
								<?php
								endif; ?>
							</header><!-- .entry-header -->
							<div class="entry-content">
								<?php
						        if ( is_single() ) :
												the_content();
						        else :
						            the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
						        endif;
								?>
							</div><!-- .entry-content -->
						</article><!-- #post-## -->
							</div>
						</div>

						<?php endwhile;

						else: ?>
						<p>Sorry, no posts matched your criteria.</p>


						<?php endif;

				?>

				</div>
			</div>

		</div><!-- .row -->
	</div><!-- .container -->
</section>

<?php

get_footer();
