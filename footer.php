<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>

	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container pt-3 pb-3">
			<div class="row">
				<div class="col-3 site-info">
					<div class="row">
						<div>
							<div class="footer-image" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
							</div>
							<p class="adress text-right">
								<?php
								if(get_theme_mod( 'header_banner_title_setting' )){
									echo get_theme_mod( 'header_banner_title_setting' );
								}else{
									echo '';
								}
								?>
								<p>

								</div><!-- close .site-info -->
						</div>
					</div>
				<div class="col-9">
					<div class="row">
						<div class="footer-links">
							<h5>Informacje o stronie</h5>
							<ul>
								<li><a href="polityka-prywatnosci">Polityka prywatności</a></li>
							</ul>
						</div>
						<div class="footer-links">
							<h5>Usługi medyczne</h5>
							<ul>
								<li><a href="specjalizacje">Usługi wg specjalizacji</a></li>
								<li><a href="lekarze">Usługi wg lekarzy</a></li>
								<li><a href="laboratorium">Laboratorium</a></li>
							</ul>
						</div>
						<div class="footer-links">
							<h5>Wizyty prywatne</h5>
							<ul>
								<li><a href="ginekologia">Ginekologia</a></li>
								<li><a href="kardiologia">Kardiologia</a></li>
								<li><a href="dermatologia">Dermatologia</a></li>
								<li><a href="urologia">Urologia</a></li>
								<li><a href="chirurgia-naczyniowa">Chirurgia naczyniowa</a></li>
							</ul>
						</div>
						<div class="footer-links">
							<h5>Świadczenia nfz</h5>
							<ul>
								<li><a href="<?php the_permalink("35"); ?>#ginekologia">Ginekologia</a></li>
								<li><a href="<?php the_permalink("35"); ?>#kardiologia">Kardiologia</a></li>
								<li><a href="<?php the_permalink("35"); ?>#">Skierowania</a></li>
							</ul>
						</div>
						<div class="footer-links">
							<h5>Przychodnia</h5>
							<ul>
								<li><a href="https://www.facebook.com/przychodniadaramed/" target="_blank">Facebook</a></li>
								<li><a href="/#adres">Kontakt</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
	<script type="text/javascript">
			// When the window has finished loading create our google map below
			google.maps.event.addDomListener(window, 'load', init);

			function init() {
					// Basic options for a simple Google Map
					// For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
					var mapOptions = {
							// How zoomed in you want the map to start at (always required)
							zoom: 16,

							// The latitude and longitude to center the map (always required)
							center: new google.maps.LatLng(53.4346866, 14.5469645), // New York

							// How you would like to style the map.
							// This is where you would paste any style found on Snazzy Maps.
							styles: [{"stylers":[{"hue":"#dd0d0d"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]}]
					};

					// Get the HTML DOM element that will contain your map
					// We are using a div with id="map" seen below in the <body>
					var mapElement = document.getElementById('map');

					// Create the Google Map using our element and options defined above
					var map = new google.maps.Map(mapElement, mapOptions);

					// Let's also add a marker while we're at it
					var marker = new google.maps.Marker({
							position: new google.maps.LatLng(53.4346866, 14.5469645),
							map: map,
							title: 'Snazzy!'
					});
			}
	</script>

	<script src="<?php echo get_template_directory_uri(); ?>/js/footer-links.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/menu.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/wow.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/price-list.js"></script>
	<script>
    new WOW().init();
  </script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('.js-example-basic-single').select2();
		$('.js-example-basic-single-secondary').select2();
		$('b[role="presentation"]').hide();
		$('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

		$('.select2').css('width', '100%').css('margin-bottom', '5px');
		$('.select2-selection').css('border', '1px solid #ec2227').css('border-radius', '3px').css('text-align', 'left');

		var uslugaSelectorsHTML = $('#usluga .js-example-basic-single');
		var uslugaButtonHTML = $('#usluga .button-header');

		$.each(uslugaSelectorsHTML, function(i, val) {
			$(this).on('select2:close', function() {
				if (i == 0) {
					if ($(this).val()) {
						$('.js-example-basic-single[name="specjalizacja"]').prop("disabled", false);
						console.log($(this).val());
					}
					if ($(this).val() == 'prywatna') {
						$('.js-example-basic-single[name="specjalizacja"]').empty();
						$('.js-example-basic-single[name="specjalizacja"]').append('<option disabled selected value>Wybierz specjalizację</option><option value="ginekologia">Ginekologia</option><option value="kardiologia">Kardiologia</option><option value="urologia">Urologia</option><option value="dermatologia">Dermatologia</option>');
					} else if ($(this).val() == 'nfz') {
						$('.js-example-basic-single[name="specjalizacja"]').empty();
						$('.js-example-basic-single[name="specjalizacja"]').append('<option disabled selected value>Wybierz specjalizację</option><option value="ginekologia">Ginekologia</option><option value="kardiologia">Kardiologia</option>');
					}
				}
			});
		});

		uslugaButtonHTML.on('click', function(e){
			e.preventDefault();
			if (uslugaSelectorsHTML.eq(0).val() == 'prywatna' && uslugaSelectorsHTML.eq(1).val()) {
				window.location.href = "<?php echo get_template_directory_uri(); ?>/"+uslugaSelectorsHTML.eq(1).val()+"/";
			} else if (uslugaSelectorsHTML.eq(0).val() == 'nfz' && uslugaSelectorsHTML.eq(1).val()) {
				window.location.href = "<?php echo get_template_directory_uri(); ?>/swiadczenia-nfz#"+uslugaSelectorsHTML.eq(1).val();
			}
		})
	});
	</script>
</body>
</html>
