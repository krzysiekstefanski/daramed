var gulp = require ('gulp');
var sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
let sourcemaps = require('gulp-sourcemaps');
gulp.task('sass', function (){
  return gulp.src('scss/style.scss', {outputStyle: 'compressed'})
    .pipe(sass({errLogToConsole: true}))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
});
gulp.task('watch', function(){
  gulp.watch('scss/**/*.scss', ['sass']);
});
