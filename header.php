<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/search-panel.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnNI0IYxjLXY74RFAcSAkF0EJmiKai4Xw&callback=initMap"
  	async defer></script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
        <div class="container">
            <nav class="navbar navbar-expand-xl p-0">
                <div class="navbar-brand">
                    <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        </a>
                    <?php else : ?>
                        <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                    <?php endif; ?>

                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-nav" aria-controls="" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <?php
                wp_nav_menu(array(
                'theme_location'    => 'primary',
                'container'       => 'div',
                'container_id'    => 'main-nav',
                'container_class' => 'collapse navbar-collapse justify-content-start',
                'menu_id'         => false,
                'menu_class'      => 'navbar-nav',
                'depth'           => 3,
                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                'walker'          => new wp_bootstrap_navwalker()
                ));
                ?>
                <div class="registration">
                  <p class="registration--text">Rejestracja</p>
                  <p class="registration--number">91 433 24 00</p>
                </div>
                <div class="social">
                  <ul class="justify-content-end">
                    <li><a href="https://www.facebook.com/przychodniadaramed/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://plus.google.com/+PrzychodniaDaramedSzczecin" target="
                      "><i class="fab fa-google-plus-g"></i></a></li>
                  </ul>
                </div>
            </nav>
        </div>
	</header><!-- #masthead -->
    <?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
      <div class="header-video">

        <video class="bg-video__content" autoplay muted loop>
          <source src="wp-content/themes/daramed/assets/daramed.mp4" type="video/mp4">
            <source src="wp-content/themes/daramed/assets/daramed.webm" type="video/webm">
            </video>
            <div class="container header-image-container">

                <div class="header-image-box">
                  <!-- <div class="header-image" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
                  </div> -->
                </div>

            </div>
            <div id="wyszukiwarka">

              <div class="container wow fadeInRight">
                <ul class="nav nav-tabs justify-content-end">
                  <li class="usluga"><a data-toggle="tab" href="#usluga" class="active show">Wyszukaj</a></li>
                </ul>
                <div class="tab-content shadow-lg">
                  <div id="usluga" class="tab-pane active">
                    <div class="service-box">
                      <p>Dzięki wyszukiwarce możesz szybko znaleźć interesującą Cię usługę, ważne informacje i cennik</p>
                      <select class="js-example-basic-single" name="typ-wizyty">
                        <option disabled selected value>Wybierz typ wizyty</option>
                        <option value="prywatna">Prywatna</option>
                        <option value="nfz">NFZ</option>
                      </select>
                      <select class="js-example-basic-single" name="specjalizacja" disabled>
                        <option disabled selected value>Wybierz specjalizację</option>
                      </select>
                      <a class="button-header button-header--white" href="#">Pokaż wyniki</a>
                    </div>
                  </div>
                </div>
                <!-- <a href="#content" class="page-scroller"><i class="fa fa-fw fa-angle-down"></i></a> -->
                <div class="header-logo wow fadeIn" data-wow-delay="1s" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
                </div>
              </div>
            </div>
      </div>

        <div class="kolcki">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <a href="./wizyty-prywatne/">
                  <div class="col-12 klocek wow fadeInUp" data-wow-delay="0.6s">
                    <h5>wizyty prywatne</h5>
                    <p>zobacz ofertę prywatnych gabinetów lekarskich w Przychodni Daramed</p>
                  </div>
                </a>
              </div>
              <div class="col-md-3">
                <a href="./uslugi-medyczne/">
                  <div class="col-12 klocek wow fadeInUp" data-wow-delay="0.6s">
                    <h5>usługi medyczne</h5>
                    <p>zobacz ofertę prywatnych gabinetów lekarskich w Przychodni Daramed</p>
                  </div>
                </a>
              </div>
              <div class="col-md-3">
                <a href="./swiadczenia-nfz/">
                  <div class="col-12 klocek wow fadeInUp" data-wow-delay="0.8s">
                    <h5>świadczenia nfz</h5>
                    <p>zobacz ofertę prywatnych gabinetów lekarskich w Przychodni Daramed</p>
                  </div>
                </a>
              </div>
              <div class="col-md-3">
                <a href="./laboratorium/">
                  <div class="col-12 klocek wow fadeInUp" data-wow-delay="1s">
                    <h5>laboratorium</h5>
                    <p>zobacz ofertę prywatnych gabinetów lekarskich w Przychodni Daramed</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div class="zakrywacz">

          </div>
        </div>
    <?php elseif(is_page_template( 'wizyty-prywatne-template.php' ) or is_page( $page = 'lekarze' ) or is_single()): ?>
      <?php if (has_post_thumbnail( $post->ID )): ?>
        <div class="header-image" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
      <?php else: ?>
        <div class="header-image">
      <?php endif; ?>
        <div class="container header-image-container position-relative" style="min-height: 60rem;">

            <div class="header-image-box">
            </div>
        </div>
      </div>
    <?php elseif (is_category()): ?>
      <?php if (has_post_thumbnail( $post->ID )): ?>
        <div class="header-image" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
      <?php else: ?>
        <div class="header-image">
      <?php endif; ?>
            <div class="container header-image-container position-relative" style="min-height: 60rem;">

                <div class="header-image-box">
                </div>

                <div class="col-md-7" id="blok-tytulowy">
                  <div class="container">
                    <div class="row">
                      <div class="col-12">
                        <?php
                        foreach((get_the_category()) as $category) {
                            echo '<h1>' . $category->cat_name . '</h1>';
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
      </div>
    <?php else: ?>
      <?php if (has_post_thumbnail( $post->ID )): ?>
        <div class="header-image" style="background-image: url('<?= get_the_post_thumbnail_url($post->ID, 'large'); ?>);">
      <?php else: ?>
        <div class="header-image">
      <?php endif; ?>
            <div class="container header-image-container position-relative" style="min-height: 60rem;">

                <div class="header-image-box">
                </div>

                <div class="col-md-7" id="blok-tytulowy">
                  <div class="container">
                    <div class="row">
                      <div class="col-12">
                        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <p>
                          <?=
                          get_post($post->ID)->post_content;
                          ?>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
      </div>

    <?php endif; ?>
	<div id="content" class="site-content">
                <?php endif; ?>
