<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header();

$query = new WP_Query( array( 'posts_per_page' => 4 ) );
?>

	<section id="specjalizacje">
		<!-- <main id="main" class="site-main" role="main"> -->

		<div class="container">
			<div class="row">

				<div class="col-12">
					<div class="row wow fadeInUp">
						<div class="mx-auto mt-5 mt-md-0">
							<h4 class="text-center title">specjalizacje</h4>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="row wow fadeInUp">
						<div class="mx-auto description">
							<p class="text-center">wybierz specjalizację, aby zapoznać się ze szczegółową ofertą diagnostyki i zabiegów</p>
						</div>
					</div>
				</div>
				<div class="col-10 m-auto">
					<div class="row justify-content-center justify-content-lg-between">
						<a href="kardiologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
								<div class="specjalizacje__img specjalizacje__img--kardiologia m-auto"></div>
								<h5 class="text-center">kardiologia</h5>
							</div>
						</a>
						<a href="ginekologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
								<div class="specjalizacje__img specjalizacje__img--ginekologia m-auto"></div>
								<h5 class="text-center">ginekologia</h5>
							</div>
						</a>
						<a href="chirurgia-naczyniowa">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
								<div class="specjalizacje__img specjalizacje__img--chirurgia-naczyniowa m-auto"></div>
								<h5 class="text-center">chirurgia naczyniowa</h5>
							</div>
						</a>
						<a href="dermatologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
								<div class="specjalizacje__img specjalizacje__img--dermatologia m-auto"></div>
								<h5 class="text-center">dermatologia</h5>
							</div>
						</a>
						<a href="urologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.6s">
								<div class="specjalizacje__img specjalizacje__img--urologia m-auto"></div>
								<h5 class="text-center">urologia</h5>
							</div>
						</a>
					</div>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->

		<!-- </main>--><!-- #main -->
	</section><!-- #primary -->
	<section id="laboratorium">

		<div class="container">
			<div class="row my-5">

				<div class="col-12 col-sm-8 col-md-5 laboratorium--text">
					<h2 class="wow fadeInUp">Laboratorium</h2>
					<p class="wow fadeInUp">Przychodnia Daramed oferuje szeroki zakres badań laboratoryjnych, Posiadamy specjalnie przygotowane profile badań m.in. dla kobiet w ciąży, kobiet po 45 roku życia, dzieci czy osób z chorobami miażdżycowymi. Na wszystkie pakiety badań nasi pacjęci dostają <span>10% rabatu</span>.</p>

					<p class="wow fadeInUp">w <span class="text-uppercase">maju</span> laboratorium czynne w dniach:</p>
					<ul class="text-color-primary wow fadeInUp">
						<li>01/05 | 03/05 | 06/05 | 07/05 | 14/05 | 21/05</li>
						<li>24/05 | 25/05 | 26/05 | 27/05 | 28/05 | 30/05</li>
					</ul>
					<p class="wow fadeInUp">w godzinach</p>
					<ul class="text-color-primary wow fadeInUp">
						<li>8:00 - 11:00</li>
					</ul>
					<a href="laboratorium" class="button-primary wow fadeInUp">zobacz ofertę</a>
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</section>
	<section id="skierowania">
		<div class="container">
			<div class="row my-5">
				<div class="col-12 col-md-6 my-5 wow fadeInLeft">
					<div class="row">
						<div class="col-12">
							<h3>wizyty prywatne</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-12 d-flex flex-column flex-md-row-reverse align-items-center justify-content-start">
							<div class="referral mb-3">
								<span class="primary-bg-span to-right">nie wymaga skierowania</span>
							</div>
							<div class="specialization mb-3">
								<div class="specialization-icon__home specialization-icon__home--urologia to-right"></div>
								<div class="specialization-icon__home specialization-icon__home--dermatologia to-right"></div>
								<div class="specialization-icon__home specialization-icon__home--chirurgia-naczyniowa to-right"></div>
								<div class="specialization-icon__home specialization-icon__home--ginekologia to-right"></div>
								<div class="specialization-icon__home specialization-icon__home--kardiologia to-right"></div>
							</div>
						</div>
					</div>
					<p>Potrzebujesz szybkiej konsultacji z lekarzem specjalistą? By zapisać się na wizytę prywatną nie musisz posiadać skierowania. Dzięki temu bez dodatkowych formalności i w krótkim terminie możesz uzyskać potrzebną Ci pomoc i poradę lekarską.</p>
					<a href="wizyty-prywatne" class="button-primary">dowiedz się więcej</a>
				</div>
				<div class="col-12 col-md-6 my-5 wow fadeInRight">
					<div class="img-wrapper shadow-sm">
						<img class="img-fluid" src="wp-content/themes/daramed/img/wizyty-prywatne.jpg" alt="">
					</div>
				</div>

				<div class="col-12 col-md-6 my-5 wow fadeInLeft">
					<div class="img-wrapper shadow-sm">
						<img class="img-fluid" src="wp-content/themes/daramed/img/swiadczenia-nfz.jpg" alt="">
					</div>
				</div>
				<div class="col-12 col-md-6 my-5 wow fadeInRight">
					<div class="row">
						<div class="col-12">
							<h3>świadczenia nfz</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-12 d-flex flex-column flex-md-row align-items-center justify-content-start">
							<div class="referral mb-3">
								<span class="primary-bg-span to-left">skierowanie wymagane do poradni kardiologicznej</span>
							</div>
							<div class="specialization mb-3">
								<div class="specialization-icon__home specialization-icon__home--ginekologia to-left"></div>
								<div class="specialization-icon__home specialization-icon__home--kardiologia to-left"></div>
							</div>
						</div>
					</div>
					<p>Przychodnia posiada podpisany kontrakt z Narodowym Funduszem Zdrowia, który umożliwia nam świadczenie usług w ramach ubezpieczenia zdrowotnego w zakresie kardiologii i ginekologii. Skontaktuj się z nami i zarezerwuj termin wizyty już dzisiaj!</p>
					<a href="swiadczenia-nfz" class="button-primary">dowiedz się więcej</a>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->

	</section>
	<section id="telefon">

		<div class="container d-flex align-items-center">
			<div class="row my-5">
				<div class="col-12">
					<div class="row">
						<div class="col-12">
							<h5><span class="bg-color-primary text-color-white phone-info shadow-lg wow fadeInUp">Informacja i rejestracja telefoniczna</span></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="bg-color-primary text-color-white phone-number mt-2 d-inline-block shadow-lg wow fadeInUp">
								91 433 24 00
							</div>
						</div>
					</div>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->

	</section>
	<section id="aktualnosci">

		<div class="container">
			<div class="row my-5">
				<div class="col-12 wow fadeInUp">
					<h2 class="text-center">aktualności</h2>
				</div>
				<div class="col-12">
					<div class="row wow fadeInUp">
						<?php
						if ( $query->have_posts() ) :

							if ( is_home() && ! is_front_page() ) : ?>
							<header>
								<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
							</header>

							<?php
						endif;

						/* Start the Loop */
						while ( $query->have_posts() ) : $query->the_post(); ?>
						<div class="col-8 col-md-3 mx-auto">
							<?php
						/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content', get_post_format() );?>
						</div>
						<?php
					endwhile;

					else :?>
						<div class="col-3">
							<?php
						get_template_part( 'template-parts/content', 'none' ); ?>
						</div>
						<?php
					endif; ?>
					</div>
				</div>
				<div class="col-12">
					<div class="row">
						<div class="m-auto">
							<a href="aktualnosci">
								<p class="text-center my-5 show-more wow fadeInUp">zobacz więcej</p>
							</a>
						</div>
					</div>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</section>
	<section id=adres>

		<div class="container">
			<div class="row my-5">

				<div class="col-md-3">
					<div class="row">
						<div class="col-12">
							<div class="row">
								<div class="col-12">
									<h5 class="text-center text-md-left"><span class="bg-color-primary text-color-white phone-info shadow-md wow fadeInUp">Informacja i rejestracja telefoniczna</span></h5>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<h5 class="text-center text-md-left">
									<div class="bg-color-primary text-color-white phone-number mt-2 shadow-md wow fadeInUp">
										91 433 24 00
									</div>
									</h5>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h4 class="text-center text-md-left wow fadeInUp">adres przychodni</h4>
							<ul class="text-center text-md-left wow fadeInUp">
								<li>ul. Mazurska 28, Szczecin</li>
								<li>+48 91 433 24 00</li>
								<li>daramed@wp.pl</li>
							</ul>
						</div>
						<div class="col-12 mb-5 mb-md-0">
							<h4 class="text-center text-md-left wow fadeInUp">godziny otwarcia przychodni</h4>
							<ul class="text-center text-md-right d-md-inline-block wow fadeInUp">
								<li>Poniedziałek: <span>8:00 - 18:00</span></li>
								<li>Wtorek: <span>8:00 - 20:00</span></li>
								<li>Środa: <span>8:00 - 18:00</span></li>
								<li>Czwartek: <span>8:00 - 18:00</span></li>
								<li>Piątek: <span>8:00 - 20:00</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9 wow fadeInUp">
					<div id="map">

					</div>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->
	</section>

<?php
get_footer();

function pbd_alp_init() {
 global $wp_query;

 // Add code to index pages.
 if( !is_singular() ) {
	 // Queue JS and CSS
	 wp_enqueue_script(
		 'pbd-alp-load-posts',
		 plugin_dir_url( __FILE__ ) . 'js/load-posts.js',
		 array('jquery'),
		 '1.0',
		 true
	 );

	 wp_enqueue_style(
		 'pbd-alp-style',
		 plugin_dir_url( __FILE__ ) . 'style.css',
		 false,
		 '1.0',
		 'all'
	 );



	 // What page are we on? And what is the pages limit?
	 $max = $wp_query->max_num_pages;
	 $paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;

	 // Add some parameters for the JS.
	 wp_localize_script(
		 'pbd-alp-load-posts',
		 'pbd_alp',
		 array(
			 'startPage' => $paged,
			 'maxPages' => $max,
			 'nextLink' => next_posts($max, false)
		 )
	 );
 }
}
add_action('template_redirect', 'pbd_alp_init');

?>
