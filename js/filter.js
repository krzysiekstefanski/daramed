$( document ).ready(function() {

	var filterHTML = $(".table-filter a");
	var showAllHTML = $(".table-show-all a");
	var tableDescriptionHTML = $(".tg-opis");
	var tableRowHTML = $("tr.tg");
	console.log(tableDescriptionHTML.html());

	filterHTML.click(function(e) {
		e.preventDefault();
		var a = $(this).text();
		tableRowHTML.each(function() {
			var b = $(this).find(".tg-opis").html();
			console.log("a: "+a);
			console.log("b: "+b);
			if ( (a == b) || ($(this).is(".table-header")) ) {
				$(this).show();
			} else {
				$(this).hide();
			}
		})
	})
	showAllHTML.click(function(e) {
		e.preventDefault();
		var a = $(this).text();
		tableRowHTML.each(function() {
				$(this).show();
		})
	})

});
