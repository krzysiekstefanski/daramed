$( document ).ready(function() {
	if ($(window).width() > 1199) {
		$(window).scroll(function(){
      if ($(this).scrollTop() > 5) {
          $('#main-nav').css('top', '0px').css('height', '100%')
          $('#masthead').css('padding-top', '0px').css('position', 'fixed');
					$('#masthead .navbar').removeClass('wow').removeClass('fadeInDown').css('visibility', 'visible');
      } else {
				$('#main-nav').css('top', '5px').css('height', 'auto')
					$('#masthead').css('padding-top', '5px').css('position', 'fixed');
					$('#masthead .navbar').addClass('wow').addClass('fadeInDown');
      }
		});
	}

});
