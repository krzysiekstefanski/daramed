$( document ).ready(function() {

	$('tr:not(:first):not(:last)').each(function(index, elem) {

		$(this).mouseenter(function() {
			var a = $(this).find('.tg-cena span').text();
			$('.tg-zaplata span').text(a);
		})

		$(this).mouseleave(function() {
			var a = $(this).find('.tg-cena span').text();
			$('.tg-zaplata span').text(0);
		})

	})

});
