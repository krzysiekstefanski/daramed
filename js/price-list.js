$( document ).ready(function() {
	var priceHTML = $('.tg-cena span');
	var checkboxHTML = $('.checkmark');
	var addedPrice;
	var overallPrice = 0;
	var sum = 0;

	checkboxHTML.each(function(){
		$(this).click(function(){
			$(this).parent().parent().parent().find(priceHTML).toggleClass('checked-price');
			overallPrice = 0;
			$('.checked-price').each(function(){
				sum = parseFloat($(this).text());
				overallPrice = overallPrice + sum;
			});
			overallPrice = parseFloat(overallPrice).toFixed(2);
			$('.tg-zaplata span').text(overallPrice);
		});
	});
});
