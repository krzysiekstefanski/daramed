$( document ).ready(function() {
	lekarzHTML = $(".lekarz").find('a');
	uslugaHTML = $(".usluga").find('a');
	contentHTML = $(".tab-content");

	lekarzHTML.on('click', function() {
		lekarzHTML.css('border-color', '#fff')
		uslugaHTML.css('border-color', '#fff')
		contentHTML.css('background-color', '#fff')
	})

	uslugaHTML.on('click', function() {
		lekarzHTML.css('border-color', '#ec2227')
		uslugaHTML.css('border-color', '#ec2227')
		contentHTML.css('background-color', '#ec2227')
	})
});
