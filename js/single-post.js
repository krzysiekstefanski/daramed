$( document ).ready(function() {
	var headerHeightHTML = $('.header-image').height();
	$('article').css('margin-top', '-'+headerHeightHTML/2+'px').css('min-height', headerHeightHTML);

	$(window).on('resize', function(){
	headerHeightHTML = $('.header-image').height();
	$('article').css('margin-top', '-'+headerHeightHTML/2+'px').css('min-height', headerHeightHTML);
	});

});
