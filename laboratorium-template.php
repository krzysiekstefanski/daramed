<?php /* Template Name: Podstrona Laboratorium */ ?>

<?php
/**
 * Template Name: Podstrona Laboratorium
 *
 * @package WordPress
 * @subpackage daramed
 * @since daramed 1.0


 */get_header(); ?>
	<section class="content-area col-12" id="strona-laboratorium">

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="row space-bottom">
							<div class="col-md-5">
								<img src="<?php header_image(); ?>" alt="daramed logo" width="100%" class="wow fadeInUp" data-wow-delay="0.2s">
								<p class="wow fadeInUp" data-wow-delay="0.2s"><?php echo get_field("opis") ?></p>
							</div>
							<div class="col-md-6 offset-md-1">
								<div class="row">
									<div class="col-12">
										<table class="calendar wow fadeInUp" data-wow-delay="0.2s">
											<h4 class="text-center wow fadeInUp" data-wow-delay="0.2s"><?php echo get_field("miesiac") ?></h4>
										  <tr class="calendar--header">
										    <th>Poniedziałek</th>
										    <th>Wtorek</th>
										    <th>Środa</th>
										    <th>Czwartek</th>
										    <th>Piątek</th>
										  </tr>
											<?php while ( have_rows('kalendarz') ) : the_row(); ?>
										  <tr>
										    <td><?= the_sub_field('poniedzialek'); ?></td>
										    <td><?= the_sub_field('wtorek'); ?></td>
												<td><?= the_sub_field('sroda'); ?></td>
										    <td><?= the_sub_field('czwartek'); ?></td>
										    <td><?= the_sub_field('piatek'); ?></td>
										  </tr>
										<?php endwhile; ?>
										</table>
										<p class="laboratory-open wow fadeInUp" data-wow-delay="0.2s">laboratorium czynne w godzinach: &nbsp; <span><?php echo get_field("godziny-otwarcia") ?></span> </p>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-12">
								<div class="row">
									<div class="col-12">
										<div class="filter-buttons mb-3">
											<div class="table-show-all m-2">
												<a href="#" class="button-primary-reversed">Wszystkie</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Diagnostyka choroby wieńcowej i chorób serca</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Hematologia</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Markery odczynów zapalnych i chorób reumatologicznych</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Serologia</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Diagnostyka chorób tarczycy</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Diagnostyka cukrzycy</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Badania podstawowe i biochemiczne</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Infekcje</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Hormony płciowe i inne badania ginekologiczne</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Diagnostyka anemii</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Diagnostyka chorób nowotworowych</a>
											</div>
											<div class="table-filter m-2">
												<a href="#" class="button-primary-reversed">Immunoglobuliny</a>
											</div>
										</div>
									</div>
								</div>
								<table class="tg wow fadeInUp" data-wow-delay="0.2s">
								  <tr class="tg table-header wow fadeIn" data-wow-delay="0.2s">
								    <th colspan="4" class="tg-nazwa-tabeli">Cennik badań laboratoryjnych</th>
								  </tr>
								  <tr class="tg table-header wow fadeIn" data-wow-delay="0.2s">
								    <th class="tg-nazwa">Nazwa usługi</th>
								    <th class="tg-opis">Krótki opis</th>
								    <th class="tg-suma">Sumuj koszt</th>
								    <th class="tg-cena">Cena</th>
								  </tr>
									<?php while ( have_rows('cennik') ) : the_row(); ?>
								  <tr class="tg">
								    <td class="tg-nazwa"><?= the_sub_field('nazwa-uslugi'); ?></td>
								    <td class="tg-opis"><?= the_sub_field('krotki-opis'); ?></td>
										<td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span><?= the_sub_field('cena'); ?></span> zł</td>
								  </tr>
									<?php endwhile; ?>
									<tr>
								    <td colspan="4" class="tg-zaplata"><span>0.00</span> zł</td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row">

							<div class="col-12">
								<div class="row wow fadeInUp">
									<div class="col-md-4 mx-auto mt-5 mt-md-0">
										<h4 class="text-center title">zobacz więcej usług</h4>
									</div>
								</div>
							</div>
							<div class="col-12">
								<div class="row wow fadeInUp">
									<div class="col-md-4 mx-auto description">
										<p class="text-center">oferujemy naszym pacjentom szeroki wachlarz usług medycznych w konkurencyjnych cenach</p>
									</div>
								</div>
							</div>
							<div class="col-10 m-auto">
								<div class="row justify-content-center justify-content-lg-between">
									<a href="kardiologia">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
											<div class="specjalizacje__img specjalizacje__img--kardiologia m-auto"></div>
											<h5 class="text-center">kardiologia</h5>
										</div>
									</a>
									<a href="ginekologia">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
											<div class="specjalizacje__img specjalizacje__img--ginekologia m-auto"></div>
											<h5 class="text-center">ginekologia</h5>
										</div>
									</a>
									<a href="chirurgia-naczyniowa">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
											<div class="specjalizacje__img specjalizacje__img--chirurgia-naczyniowa m-auto"></div>
											<h5 class="text-center">chirurgia naczyniowa</h5>
										</div>
									</a>
									<a href="dermatologia">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
											<div class="specjalizacje__img specjalizacje__img--dermatologia m-auto"></div>
											<h5 class="text-center">dermatologia</h5>
										</div>
									</a>
									<a href="urologia">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.6s">
											<div class="specjalizacje__img specjalizacje__img--urologia m-auto"></div>
											<h5 class="text-center">urologia</h5>
										</div>
									</a>
								</div>
							</div>

						</div><!-- .row -->
					</div>
				</div>
			</div>

	</section><!-- #primary -->

	<script src="<?php echo get_template_directory_uri(); ?>/js/filter.js"></script>
<?php
get_footer();
