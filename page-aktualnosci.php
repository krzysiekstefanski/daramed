<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

add_filter( 'the_content', 'wpse_280633_break_text' );


get_header(); ?>

<?php

// example args
$args = array(
	'post_type' => 'post',
	'post_status' => 'publish',
	'posts_per_page' => '20',
	'paged' => get_query_var('paged')
);

// the query
$the_query = new WP_Query( $args );

?>
<section id="aktualnosci">

	<div class="container">
		<div class="row my-5">
			<div class="col-12">
				<div class="row wow fadeInUp">
					<?php
					if ( $the_query->have_posts() ) :

						if ( is_home() && ! is_front_page() ) : ?>
						<header>
							<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
						</header>

						<?php
					endif;

					/* Start the Loop */
					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
					<div class="col-8 col-md-3 mx-auto mx-md-0">
						<?php
					/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content-wdsc', get_post_format() );?>
					</div>
					<?php
				endwhile;

				the_posts_navigation();
				wp_reset_postdata();

				else :?>
					<div class="col-3">
						<?php
					get_template_part( 'template-parts/content-wdsc', 'none' ); ?>
					</div>
					<?php
				endif; ?>
				</div>
			</div>

		</div><!-- .row -->
	</div><!-- .container -->
</section>

<?php

get_footer();
