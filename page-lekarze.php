<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
get_header(); ?>

<?php

/*
 * Tablica zawierająca uporządkowanych według specjalizacji lekarzy wraz z danymi. Do późniejszego wykorzystania.
 */
$sortableField = [];

/*
 * Zmienne przechowujące parametry pobranych pól z ACF
 */
$specializationsArray = get_field_object('field_5b56d61357214'); //ID pola wyboru specjalizacji

if (have_rows('lekarz')):

    foreach ($specializationsArray['choices'] as $specializationKey => $specialization):

        /*
         * Tablica przechowująca lekarzy w pętli. Resetowana przy każdym przejściu pętli. Tablica tymczasowa.
         */
        $doctorsArray = [];

        while (have_rows('lekarz')) : the_row();

            $select = get_sub_field_object('specjalizacja'); //Nazwa pola specjalizacji
            $values = $select['value'];

            if ($values):

                foreach($values as $value){

                    if ($value == $specializationKey) {

												array_push($doctorsArray, [
													'doctor_name' => get_sub_field('lekarz_nazwa'),
													'tags' => get_sub_field('grupa-tagow'),
													'terms' => get_sub_field('terminy-przyjec'),

												]);

                    }

                }

            endif;

        endwhile;


        $sortableFieldTemporary = [
            'specialization_id' => $specializationKey,
            'name' => $specialization,
            'doctors' => $doctorsArray
        ];

        array_push($sortableField, $sortableFieldTemporary);

    endforeach;

endif;

?>

<section class="content-area col-12" id="strona-lekarze">

		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
				<div class="col-md-9">
          <?php
						foreach ($sortableField as $value) :
							if (count($value["doctors"]) > 0 ) : ?>
								<div class="doctor-specialization-component">
    						<div class="row wow fadeInUp" data-wow-delay="0.2s">
    							<div class="col-12">
    								<div class="border-bottom d-flex">
    									<div class="specialization-icon__home specialization-icon__home--<?= $value["name"]; ?>"></div>
    									<h5 class="my-2 ml-3"><?= $value["name"]; ?></h5>
    								</div>
    							</div>
    						</div>
    						<?php
    						foreach ($value["doctors"] as $doctor) :
    						?>
						<div class="row doctor-box wow fadeIn" data-wow-delay="0.4s">
							<div class="col-md-6">
								<div class="row">
									<div class="col-12">
										<div class="doctor-profile medical-stethoscope">
											<p class="text--normal"><?= $doctor['doctor_name']; ?></p>
										</div>
										<div class="doctor-profile">
                        <?php foreach ($doctor["tags"] as $tag) : ?>
												  <span class="primary-bg-span text--small"><?= $tag["tag"]; ?></span>
                        <?php endforeach ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row mb-3 mt-2">
									<?php foreach ($doctor["terms"] as $term) : ?>
									<div class="col-3 col-sm-2 col-md-3 pl-md-2 admission">
										<p class="text--small text-center w-100 admission--day"><?= $term["dzien-przyjec"]; ?></p>
										<p class="text--small text-center w-100 admission--time"><?= $term["godziny-przyjec"]; ?>
                      <?php if ($term["nfz"] == 1) : ?>
                      <span>NFZ</span>
                      <?php endif ?>
									</div>
									<?php endforeach ?>
								</div>
							</div>
						</div>
						<?php
						endforeach;
						 ?>
					</div>
					<?php
							endif;
						?>
					<?php
					endforeach;
					?>
					<div class="row">
						<div class="col-12">
							<table class="tg wow fadeInUp" data-wow-delay="0.2s">
							  <tr class="wow fadeIn" data-wow-delay="0.2s">
							    <th class="tg-nazwa">Nazwa usługi</th>
							    <th class="tg-opis">Krótki opis</th>
							    <th class="tg-cena">Cena</th>
							  </tr>
                <?php while ( have_rows('cennik') ) : the_row(); ?>
							  <tr class="wow fadeIn" data-wow-delay="0.2s">
							    <td class="tg-nazwa"><?= the_sub_field('nazwa-uslugi'); ?></td>
							    <td class="tg-opis"><?= the_sub_field('krotki-opis'); ?></td>
							    <td class="tg-cena"><span><?= the_sub_field('cena'); ?></span> zł</td>
							  </tr>
                <?php endwhile; ?>
								<tr class="wow fadeIn" data-wow-delay="0.2s">
							    <td colspan="4" class="tg-zaplata"><span>0</span> zł</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

</section><!-- #primary -->

<script src="<?php echo get_template_directory_uri(); ?>/js/price-change-on-hover.js"></script>
<?php
get_footer();
