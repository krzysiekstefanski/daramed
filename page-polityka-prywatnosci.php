<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section class="content-area col-12" id="strona-polityka-prywatnosci">

			<div class="container">

				<div class="row justify-content-center nfz-clinic-component">
					<div class="col-11">
						<div class="row">
              <article id="post-479" class="post-479 page type-page status-publish hentry">
                <header class="entry-header">
                  <h1 class="entry-title">Informacje o&nbsp;plikach cookies</h1> </header>
                <div class="entry-content">
                  <div data-box-name="article.title">
                    <div class="markdown-text">
                      <h2 id="polityka-plikow-cookies">Polityka plików „cookies”</h2></div>
                  </div>
                  <div>
                    <div class="markdown-text">
                      <p><strong>Niniejsza Polityka dotyczy plików „cookies” i&nbsp;odnosi się do stron internetowych, których operatorem jest Daramed sp. z&nbsp;o.o. z&nbsp;siedzibą w&nbsp;Szczecinie (zwanych dalej: „stronami internetowymi”).</strong></p>

                      <h4 id="czym-sa-pliki-cookies">Czym są pliki „cookies”?</h4>

                      <p>Poprzez pliki „cookies” należy rozumieć dane informatyczne, w&nbsp;szczególności pliki tekstowe, przechowywane w&nbsp;urządzeniach końcowych użytkowników przeznaczone do korzystania ze stron internetowych. Pliki te pozwalają rozpoznać urządzenie
                      użytkownika i&nbsp;odpowiednio wyświetlić stronę internetową dostosowaną do jego indywidualnych preferencji. „Cookies” zazwyczaj zawierają nazwę strony internetowej z&nbsp;której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz
                      unikalny numer.</p>

                      <h4 id="do-czego-uzywamy-plikow-cookies">Do czego używamy plików „cookies”?</h4>

                      <p>Pliki „cookies” używane są w&nbsp;celu dostosowania zawartości stron internetowych do preferencji użytkownika oraz optymalizacji korzystania ze stron internetowych. Używane są również w&nbsp;celu tworzenia anonimowych, zagregowanych statystyk,
                      które pomagają zrozumieć w&nbsp;jaki sposób użytkownik korzysta ze stron internetowych co umożliwia ulepszanie ich struktury i&nbsp;zawartości, z&nbsp;wyłączeniem personalnej identyfikacji użytkownika.</p>

                      <h4 id="jakich-plikow-cookies">Jakich plików „cookies” używamy?</h4>

                      <p>Stosowane są, co do zasady, dwa rodzaje plików „cookies” – „sesyjne” oraz „stałe”. Pierwsze z&nbsp;nich są plikami tymczasowymi, które pozostają na urządzeniu użytkownika, aż do wylogowania ze strony internetowej lub wyłączenia oprogramowania
                      (przeglądarki internetowej). „Stałe” pliki pozostają na urządzeniu użytkownika przez czas określony w&nbsp;parametrach plików „cookies” albo do momentu ich ręcznego usunięcia przez użytkownika. Pliki „cookies” wykorzystywane przez partnerów
                      operatora strony internetowej, w&nbsp;tym w&nbsp;szczególności użytkowników strony internetowej, podlegają ich własnej polityce prywatności.</p>

                      <p>Wyróżnić można szczegółowy podział cookies, ze względu na:</p>

                      <h5 id="a.-ze-wzgledu-na-niezbednosc-do-realizacji-uslugi">A. Ze względu na niezbędność do realizacji usługi</h5>

                      <p><strong>Niezbędne</strong>&nbsp;– są absolutnie niezbędne do prawidłowego funkcjonowania witryny lub funkcjonalności z&nbsp;których użytkownik chce skorzystać.</p>

                      <p><strong>Funkcjonalne</strong>&nbsp;– są ważne dla działania serwisu:</p>

                      <ul>
                        <li>służą wzbogaceniu funkcjonalności serwisu, bez nich serwis będzie działał poprawnie, jednak nie będzie dostosowany do preferencji użytkownika,</li>
                        <li>służą zapewnieniu wysokiego poziomu funkcjonalności serwisu, bez ustawień zapisanych w&nbsp;plikach cookies może obniżyć się poziom funkcjonalności witryny, ale nie powinna uniemożliwić zupełnego korzystania z&nbsp;niej,</li>
                        <li>służą bardzo ważnym funkcjonalnościom serwisu, ich zablokowanie spowoduje, że wybrane funkcje nie będą działać prawidłowo.</li>
                      </ul>

                      <p><strong>Biznesowe</strong>&nbsp;– umożliwiają realizację modelu biznesowego w&nbsp;oparciu o&nbsp;który udostępniona jest witryna, ich zablokowanie nie spowoduje niedostępności całości funkcjonalności serwisu ale może obniżyć poziom świadczenia
                      usługi ze względu na brak możliwości realizacji przez właściciela witryny przychodów subsydiujących działanie serwisu. Do tej kategorii należą np. cookies reklamowe.</p>

                      <h5 id="b.-ze-wzgledu-na-czas-przez-jaki-cookies-beda-umieszczone-w-urzadzeniu-koncowym-uzytkownika">B. Ze względu na czas przez jaki cookies będą umieszczone w urządzeniu końcowym użytkownika</h5>

                      <p><strong>Cookies tymczasowe (session cookies)</strong>&nbsp;– cookies umieszczone na czas korzystania z&nbsp;przeglądarki (sesji), zostają wykasowane po jej zamknięciu.</p>

                      <p><strong>Cookies stałe (persistent cookies)&nbsp;</strong>– nie są kasowane po zamknięciu przeglądarki i&nbsp;pozostają w&nbsp;urządzeniu użytkownika na określony czas lub bez okresu ważności w&nbsp;zależności od ustawień właściciela witryny.</p>

                      <h5 id="c.-ze-wzgledu-na-pochodzenie-administratora-serwisu-ktory-zarzadza-cookies">C. Ze względu na pochodzenie – administratora serwisu, który zarządza cookies</h5>

                      <p><strong>Cookies własne (first party cookies)&nbsp;</strong>– cookies umieszczone bezpośrednio przez właściciela witryny jaka została odwiedzona.</p>

                      <p><strong>Cookies zewnętrzne (third-party cookies)</strong>&nbsp;– cookies umieszczone przez zewnętrzne podmioty, których komponenty stron zostały wywołane przez właściciela witryny.</p>

                      <p><strong>Uwaga:</strong>&nbsp;cookies mogą być wywołane przez administratora za pomocą skryptów, komponentów, które znajdują się na serwerach partnera, umiejscowionych w&nbsp;innej lokalizacji – innym kraju lub nawet zupełnie innym systemie prawnym.
                      W&nbsp;przypadku wywołania przez administratora witryny komponentów serwisu pochodzących spoza systemu administratora mogą obowiązywać inne standardowe zasady polityki cookies niż polityka prywatności / cookies administratora witryny.</p>

                      <h5 id="d.-ze-wzgledu-na-cel-jakiemu-sluza">D. Ze względu na cel jakiemu służą</h5>

                      <p><strong>Konfiguracji serwisu</strong>&nbsp;– umożliwiają ustawienia funkcji i&nbsp;usług w&nbsp;serwisie.</p>

                      <p><strong>Bezpieczeństwo i&nbsp;niezawodność serwisu</strong>&nbsp;– umożliwiają weryfikację autentyczności oraz optymalizację wydajności serwisu.</p>

                      <p><strong>Uwierzytelnianie</strong>&nbsp;– umożliwiają informowanie gdy użytkownik jest zalogowany, dzięki czemu witryna może pokazywać odpowiednie informacje i&nbsp;funkcje.</p>

                      <p><strong>Stan sesji</strong>&nbsp;– umożliwiają zapisywanie informacji o&nbsp;tym, jak użytkownicy korzystają z&nbsp;witryny. Mogą one dotyczyć najczęściej odwiedzanych stron lub ewentualnych komunikatów o&nbsp;błędach wyświetlanych na niektórych
                      stronach. Pliki cookies służące do zapisywania tzw. „stanu sesji” pomagają ulepszać usługi i&nbsp;zwiększać komfort przeglądania stron.</p>

                      <p><strong>Procesy</strong>&nbsp;– umożliwiają sprawne działanie samej witryny oraz dostępnych na niej funkcji.</p>

                      <p><strong>Reklamy</strong>&nbsp;– umożliwiają wyświetlać reklamy, które są bardziej interesujące dla użytkowników, a&nbsp;jednocześnie bardziej wartościowe dla wydawców i&nbsp;reklamodawców, personalizować reklamy, mogą być używane również do
                      wyświetlania reklam poza stronami witryny (domeny).</p>

                      <p><strong>Lokalizacja</strong>&nbsp;– umożliwiają dostosowanie wyświetlanych informacji do lokalizacji użytkownika.</p>

                      <p><strong>Analizy i&nbsp;badania, audyt oglądalności</strong>&nbsp;– umożliwiają właścicielom witryn lepiej zrozumieć preferencje ich użytkowników i&nbsp;poprzez analizę ulepszać i&nbsp;rozwijać produkty i&nbsp;usługi. Zazwyczaj właściciel witryny
                      lub firma badawcza zbiera anonimowo informacje i&nbsp;przetwarza dane na temat trendów bez identyfikowania danych osobowych poszczególnych użytkowników.</p>

                      <h5 id="e.-ze-wzgledu-na-ingerencje-w-prywatnosc-uzytkownika">E. Ze względu na ingerencję w prywatność użytkownika</h5>

                      <p><strong>Nieszkodliwe</strong>&nbsp;– Obejmuje cookies:</p>

                      <ul>
                        <li>niezbędne do poprawnego działania witryny,</li>
                        <li>potrzebne do umożliwienia działania funkcjonalności witryny, jednak ich działanie nie ma nic wspólnego z&nbsp;śledzeniem użytkownika.</li>
                      </ul>

                      <p><strong>Badające</strong>&nbsp;– wykorzystywane do śledzenia użytkowników jednak nie obejmują informacji pozwalających zidentyfikować danych konkretnego użytkownika.</p>

                      <h4 id="czy-pliki-cookies">Czy pliki „cookies” zawierają dane osobowe</h4>

                      <p>Dane osobowe gromadzone przy użyciu plików „cookies” mogą być zbierane wyłącznie w&nbsp;celu wykonywania określonych funkcji na rzecz użytkownika. Takie dane są zaszyfrowane w&nbsp;sposób uniemożliwiający dostęp do nich osobom nieuprawnionym.</p>

                      <h4 id="usuwanie-plikow-cookies">Usuwanie plików „cookies”</h4>

                      <p>Standardowo oprogramowanie służące do przeglądania stron internetowych domyślnie dopuszcza umieszczanie plików „cookies” na urządzeniu końcowym. Ustawienia te mogą zostać zmienione w&nbsp;taki sposób, aby blokować automatyczną obsługę plików
                      „cookies” w&nbsp;ustawieniach przeglądarki internetowej bądź informować o&nbsp;ich każdorazowym przesłaniu na urządzenie użytkownika. Szczegółowe informacje o&nbsp;możliwości i&nbsp;sposobach obsługi plików „cookies” dostępne są w&nbsp;ustawieniach
                      oprogramowania (przeglądarki internetowej). Ograniczenie stosowania plików „cookies”, może wpłynąć na niektóre funkcjonalności dostępne na stronie internetowej.</p>
                    </div>
                  </div>
                </div>
              </article>
            </div>
					</div>
				</div>

			</div>

	</section><!-- #primary -->

<?php
get_footer();
