<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section class="content-area col-12" id="strona-specjalizacje">

			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<?php get_sidebar(); ?>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div class="col-10 m-auto pb-5">
								<div class="row justify-content-center justify-content-lg-between">
									<a href="kardiologia">
										<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
											<div class="specjalizacje__img specjalizacje__img--kardiologia m-auto"></div>
											<h5 class="text-center">kardiologia</h5>
										</div>
									</a>
									<a href="<?php the_permalink("35"); ?>#kardiologia">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
										<div class="specjalizacje__img specjalizacje__img--kardiologia-nfz m-auto"></div>
										<h5 class="text-center">kardiologia NFZ</h5>
									</div>
									</a>
									<a href="ginekologia">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
										<div class="specjalizacje__img specjalizacje__img--ginekologia m-auto"></div>
										<h5 class="text-center">ginekologia</h5>
									</div>
									</a>
									<a href="<?php the_permalink("35"); ?>#ginekologia">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
										<div class="specjalizacje__img specjalizacje__img--ginekologia-nfz m-auto"></div>
										<h5 class="text-center">ginekologia NFZ</h5>
									</div>
									</a>
								</div>
							</div>
							<div class="col-10 m-auto">
								<div class="row justify-content-center justify-content-lg-between">
									<a href="chirurgia-naczyniowa">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
										<div class="specjalizacje__img specjalizacje__img--chirurgia-naczyniowa m-auto"></div>
										<h5 class="text-center">chirurgia naczyniowa</h5>
									</div>
									</a>
									<a href="dermatologia">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
										<div class="specjalizacje__img specjalizacje__img--dermatologia m-auto"></div>
										<h5 class="text-center">dermatologia</h5>
									</div>
									</a>
									<a href="urologia">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
										<div class="specjalizacje__img specjalizacje__img--urologia m-auto"></div>
										<h5 class="text-center">urologia</h5>
									</div>
									</a>
									<a href="laboratorium">
									<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
										<div class="specjalizacje__img specjalizacje__img--laboratorium m-auto"></div>
										<h5 class="text-center">laboratorium</h5>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section><!-- #primary -->

<?php
get_footer();
