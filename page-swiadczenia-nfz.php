<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<?php

/*
 * Tablica zawierająca uporządkowanych według specjalizacji lekarzy wraz z danymi. Do późniejszego wykorzystania.
 */
$sortableField = [];

/*
 * Zmienne przechowujące parametry pobranych pól z ACF
 */
$specializationsArray = get_field_object('field_5b56d61357214'); //ID pola wyboru specjalizacji

if (have_rows('lekarz')):

    foreach ($specializationsArray['choices'] as $specializationKey => $specialization):

        /*
         * Tablica przechowująca lekarzy w pętli. Resetowana przy każdym przejściu pętli. Tablica tymczasowa.
         */
        $doctorsArray = [];

        while (have_rows('lekarz')) : the_row();

            $select = get_sub_field_object('specjalizacja'); //Nazwa pola specjalizacji
            $values = $select['value'];

            if ($values):

                foreach($values as $value){

                    if ($value == $specializationKey) {

												array_push($doctorsArray, [
													'doctor_name' => get_sub_field('lekarz_nazwa'),
													'tags' => get_sub_field('grupa-tagow'),
													'terms' => get_sub_field('terminy-przyjec'),

												]);

                    }

                }

            endif;

        endwhile;


        $sortableFieldTemporary = [
            'specialization_id' => $specializationKey,
            'name' => $specialization,
            'doctors' => $doctorsArray
        ];

        array_push($sortableField, $sortableFieldTemporary);

    endforeach;

endif;
$ginekologia = get_field('ginekologia');
$kardiologia = get_field('kardiologia');
?>

	<section class="content-area col-12" id="strona-swiadczenia-nfz">

			<div class="container">
				<a class="anchor" id="ginekologia"></a>

				<div class="row justify-content-center nfz-clinic-component">
					<div class="col-11">
						<div class="row">
							<div class="col-lg-4 wow fadeInUp" data-wow-delay="0.2s">
								<?php if( $ginekologia ): ?>
								<div class="row mb-2">
									<div class="col-12 d-flex">
										<div class="specialization-icon__nfz specialization-icon__nfz--ginekologia"></div>
										<h4 class="my-2 ml-3">Ginekologia</h4>
									</div>
								</div>
								<div class="row mb-1">
									<div class="col-12">
										<span class="primary-bg-span text--normal">nie wymaga skierowania</span>
									</div>
								</div>
								<!-- <div class="row mb-1">
									<div class="col-12 d-flex">
										<p class="free-term text--normal mr-2">pierwszy wolny termin</p>
										<span class="primary-bg-span text--normal nfz-span-small"><?php echo $ginekologia['ginekologia-pierwszy-wolny-termin'] ?></span>
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-12 d-flex">
										<p class="text--small mb-0 mr-2">Stan na dzień</p>
										<span class="primary-bg-span text--small nfz-span-small"><?php echo $ginekologia['ginekologia-stan-na-dzien'] ?></span>
									</div>
								</div> -->
								<div class="row mt-5">
									<div class="col-12 nfz-circle">
											<p class="text--small">Wizyta refundowana przez NFZ <span>nie wymaga skierowania</span>. Aby zarezerwować termin wizyty należy skontaktować się z nami <b>telefonicznie</b> lub <b>przyjść osobiście</b> i umówić się na jeden z dostępnych terminów</p>
											<p class="text--small">W przypadku rezygnacji z konsultacji, należy najszybciej jak to możliwe poinformować przychodnię o nieobecności.</p>
									</div>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-8">
					          <?php
											foreach ($sortableField as $value) :
												if (count($value["doctors"]) > 0 and ($value["name"]) == 'ginekologia' ) : ?>
													<div class="doctor-specialization-component">
					    						<?php
					    						foreach ($value["doctors"] as $doctor) :
					    						?>
											<div class="row doctor-box wow fadeInUp" data-wow-delay="0.2s">
												<div class="col-6">
													<div class="row">
														<div class="col-12">
															<div class="doctor-profile medical-stethoscope">
																<p class="text--normal"><?= $doctor['doctor_name']; ?></p>
															</div>
															<div class="doctor-profile">
					                        <?php foreach ($doctor["tags"] as $tag) : ?>
																	  <span class="primary-bg-span text--small"><?= $tag["tag"]; ?></span>
					                        <?php endforeach ?>
															</div>
														</div>
													</div>
												</div>
												<div class="col-6">
													<div class="row mb-3 mt-2">
														<?php foreach ($doctor["terms"] as $term) : ?>
														<div class="col-3 pl-2 admission">
															<p class="text--small text-center w-100 admission--day"><?= $term["dzien-przyjec"]; ?></p>
															<p class="text--small text-center w-100 admission--time"><?= $term["godziny-przyjec"]; ?>
					                      <?php if ($term["nfz"] == 1) : ?>
					                      <span>NFZ</span>
					                      <?php endif ?>
														</div>
														<?php endforeach ?>
													</div>
												</div>
											</div>
											<?php
											endforeach;
											 ?>
										</div>
										<?php
												endif;
											?>
										<?php
										endforeach;
										?>
							</div>
						</div>
					</div>
				</div>
				<a class="anchor" id="kardiologia"></a>
				<div class="row justify-content-center nfz-clinic-component">
					<div class="col-11">
						<div class="row">
							<div class="col-lg-4 wow fadeInUp" data-wow-delay="0.2s">
								<?php if( $kardiologia ): ?>
								<div class="row mb-2">
									<div class="col-12 d-flex">
										<div class="specialization-icon__nfz specialization-icon__nfz--ginekologia"></div>
										<h4 class="my-2 ml-3">Kardiologia</h4>
									</div>
								</div>
								<div class="row mb-1">
									<div class="col-12">
										<span class="primary-bg-span text--normal">wymaga skierowania</span>
									</div>
								</div>
								<!-- <div class="row mb-1">
									<div class="col-12 d-flex">
										<p class="free-term text--normal mr-2">pierwszy wolny termin</p>
										<span class="primary-bg-span text--normal nfz-span-small"><?php echo $kardiologia['kardiologia-pierwszy-wolny-termin'] ?></span>
									</div>
								</div>
								<div class="row mb-5">
									<div class="col-12 d-flex">
										<p class="text--small mb-0 mr-2">Stan na dzień</p>
										<span class="primary-bg-span text--small nfz-span-small"><?php echo $kardiologia['kardiologia-stan-na-dzien'] ?></span>
									</div>
								</div> -->
								<div class="row mt-5">
									<div class="col-12 nfz-circle">
										<p class="text--small">Wizyta u specjalisty kardiologa refundowana przez NFZ <span>wymaga skierowania od lekarza POZ</span>. Aby zarezerwować termin wizyty należy <b>przyjść osobiście ze skierowaniem</b> lub zadzwonić i umówić się na jeden z dostępnych terminów, a następnie w ciągu <span>14 dni dostarczyć skierowanie do przychodni</span>.</p>
										<p class="text--small">W przypadku rezygnacji z konsultacji, należy najszybciej jak to możliwe poinformować przychodnię o nieobecności.</p>
									</div>
								</div>
								<?php endif; ?>
							</div>
							<div class="col-lg-8">
							<?php
								foreach ($sortableField as $value) :
									if (count($value["doctors"]) > 0 and ($value["name"]) == 'kardiologia' ) : ?>
										<div class="doctor-specialization-component">
										<?php
										foreach ($value["doctors"] as $doctor) :
										?>
								<div class="row doctor-box wow fadeInUp" data-wow-delay="0.2s">
									<div class="col-6">
										<div class="row">
											<div class="col-12">
												<div class="doctor-profile medical-stethoscope">
													<p class="text--normal"><?= $doctor['doctor_name']; ?></p>
												</div>
												<div class="doctor-profile">
														<?php foreach ($doctor["tags"] as $tag) : ?>
															<span class="primary-bg-span text--small"><?= $tag["tag"]; ?></span>
														<?php endforeach ?>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6">
										<div class="row mb-3 mt-2">
											<?php foreach ($doctor["terms"] as $term) : ?>
											<div class="col-3 pl-2 admission">
												<p class="text--small text-center w-100 admission--day"><?= $term["dzien-przyjec"]; ?></p>
												<p class="text--small text-center w-100 admission--time"><?= $term["godziny-przyjec"]; ?>
													<?php if ($term["nfz"] == 1) : ?>
													<span>NFZ</span>
													<?php endif ?>
											</div>
											<?php endforeach ?>
										</div>
									</div>
								</div>
								<?php
								endforeach;
								 ?>
							</div>
							<?php
									endif;
								?>
							<?php
							endforeach;
							?>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section><!-- #primary -->
	<section id="specjalizacje">
		<!-- <main id="main" class="site-main" role="main"> -->

		<div class="container">
			<div class="row">

				<div class="col-12">
					<div class="row wow fadeInUp">
						<div class="mx-auto mt-5 mt-md-0">
							<h4 class="text-center title">zobacz inne usługi w naszej przychodni</h4>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="row wow fadeInUp">
						<div class="mx-auto description">
							<p class="text-center">oferujemy naszym pacjentom szeroki wachlarz usług medycznych w konkurencyjnych cenach</p>
						</div>
					</div>
				</div>
				<div class="col-10 m-auto">
					<div class="row justify-content-center justify-content-lg-between">
						<a href="kardiologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
								<div class="specjalizacje__img specjalizacje__img--kardiologia m-auto"></div>
								<h5 class="text-center">kardiologia</h5>
							</div>
						</a>
						<a href="ginekologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
								<div class="specjalizacje__img specjalizacje__img--ginekologia m-auto"></div>
								<h5 class="text-center">ginekologia</h5>
							</div>
						</a>
						<a href="chirurgia-naczyniowa">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
								<div class="specjalizacje__img specjalizacje__img--chirurgia-naczyniowa m-auto"></div>
								<h5 class="text-center">chirurgia naczyniowa</h5>
							</div>
						</a>
						<a href="dermatologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
								<div class="specjalizacje__img specjalizacje__img--dermatologia m-auto"></div>
								<h5 class="text-center">dermatologia</h5>
							</div>
						</a>
						<a href="urologia">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.6s">
								<div class="specjalizacje__img specjalizacje__img--urologia m-auto"></div>
								<h5 class="text-center">urologia</h5>
							</div>
						</a>
						<a href="laboratorium">
							<div class="specjalizacje wow fadeInUp" data-wow-delay="0.7s">
								<div class="specjalizacje__img specjalizacje__img--laboratorium m-auto"></div>
								<h5 class="text-center">laboratorium</h5>
							</div>
						</a>
					</div>
				</div>

			</div><!-- .row -->
		</div><!-- .container -->

		<!-- </main>--><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
