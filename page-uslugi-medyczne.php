<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section class="content-area col-12" id="uslugi-medyczne">

			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<?php get_sidebar(); ?>
					</div>
					<div class="col-md-9">
						<div class="row mb-2 wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12 d-flex">
								<div class="specialization-icon__nfz specialization-icon__nfz--ginekologia"></div>
								<h4 class="my-2 ml-3">Ginekologia</h4>
								<div>
									<span class="primary-bg-span text--normal">krótkie terminy</span>
								</div>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<h5>Lekarze</h5>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-5">
								<div class="row">
									<div class="col-12">
										<div class="doctor-profile medical-stethoscope">
											<p class="text--small">lek. med. Dariusz Dębowski</p>
										</div>
										<div class="doctor-profile">
											<span class="primary-bg-span text--small">usg</span>
											<span class="primary-bg-span text--small">prowadzenie ciąży</span>
											<span class="primary-bg-span text--small">konsultacje</span>
											<span class="primary-bg-span text--small">wizyty prywatne</span>
											<span class="primary-bg-span text--small">nfz</span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-7">
								<div class="row mb-3 mt-2">
									<div class="col-3 pl-2 admission">
										<p class="text--small text-center w-100 admission--day">Wtorek</p>
										<p class="text--small text-center w-100 admission--time">17:00 - 19:00</p>
									</div>
									<div class="col-3 pl-2 admission">
										<p class="text--small text-center w-100 admission--day">Czwartek</p>
										<p class="text--small text-center w-100 admission--time">17:00 - 19:00</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<h5>Usługi</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-10 d-flex flex-wrap justify-content-center justify-content-md-start justify-content-lg-between" id="specjalizacje">
								<div class="specjalizacje wow fadeInUp" data-wow-delay="0.2s">
									<div class="specjalizacje__ginekologia-img specjalizacje__ginekologia-img--konsultacje m-auto"></div>
									<p class="text-center">konsultacje ginekologiczne</p>
								</div>
								<div class="specjalizacje wow fadeInUp" data-wow-delay="0.3s">
									<div class="specjalizacje__ginekologia-img specjalizacje__ginekologia-img--prowadzenie m-auto"></div>
									<p class="text-center">prowadzenie ciąży</p>
								</div>
								<div class="specjalizacje wow fadeInUp" data-wow-delay="0.4s">
									<div class="specjalizacje__ginekologia-img specjalizacje__ginekologia-img--usg2d m-auto"></div>
									<p class="text-center">USG 2D<br>(ginekologiczne i ciąży)</p>
								</div>
								<div class="specjalizacje wow fadeInUp" data-wow-delay="0.5s">
								<div class="specjalizacje__ginekologia-img specjalizacje__ginekologia-img--piersi m-auto"></div>
								<p class="text-center">USG piersi</p>
							</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<h5>Cennik</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<table class="tg">
								  <tr>
								    <th class="tg-nazwa">Nazwa usługi</th>
								    <th class="tg-opis">Krótki opis</th>
								    <th class="tg-suma">Sumuj koszt</th>
								    <th class="tg-cena">Cena</th>
								  </tr>
								  <tr>
								    <td class="tg-nazwa">Konsultacja ginekologiczna</td>
								    <td class="tg-opis">Wybierz specjalizację, aby zapoznać się ze szczegółową ofertą diagnostyki i zabiegów</td>
										<td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span>61.50</span> zł</td>
								  </tr>
								  <tr>
								    <td class="tg-nazwa">USG piersi</td>
								    <td class="tg-opis">Wybierz specjalizację, aby zapoznać się ze szczegółową ofertą diagnostyki i zabiegów</td>
										<td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span>36.90</span> zł</td>
								  </tr>
								  <tr>
								    <td class="tg-nazwa">USG 2D ginekologiczne</td>
								    <td class="tg-opis">Wybierz specjalizację, aby zapoznać się ze szczegółową ofertą diagnostyki i zabiegów</td>
										<td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span>61.50</span> zł</td>
								  </tr>
								  <tr>
								    <td class="tg-nazwa">Pakiet badań ciążowych</td>
								    <td class="tg-opis">Wybierz specjalizację, aby zapoznać się ze szczegółową ofertą diagnostyki i zabiegów</td>
								    <td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span>318.00</span> zł</td>
								  </tr>
									<tr>
								    <td colspan="4" class="tg-zaplata"><span>0.00</span> zł</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section><!-- #primary -->

<?php
get_footer();
