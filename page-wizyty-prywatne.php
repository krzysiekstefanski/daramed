<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section class="content-area col-12" id="nfz-świadczenia">

			<div class="container">
				<div class="col-3">
					<?php get_sidebar(); ?>
				</div>
				<div class="col-9">

				</div>
			</div>

	</section><!-- #primary -->
	<div class="mb-1000">

	</div>

<?php
get_footer();
