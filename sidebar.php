<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area col-12" role="complementary">
	<!-- <?php dynamic_sidebar( 'sidebar-1' ); ?> -->
	<?php
	wp_nav_menu(array(
	'theme_location'    => 'sidebar',
	'container'       => 'div',
	'container_id'    => 'sidebar-nav',
	'container_class' => 'brak',
	'menu_id'         => false,
	'menu_class'      => 'brak',
	'depth'           => 3,
	'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
	'walker'          => new wp_bootstrap_navwalker()
	));
	?>
</aside><!-- #secondary -->
