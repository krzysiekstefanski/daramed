<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

 get_header(); ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main" role="main">

 		<?php
 		while ( have_posts() ) : the_post();

 			get_template_part( 'template-parts/content-post', get_post_format() );


 		endwhile; // End of the loop.
 		?>

 		</main><!-- #main -->
 	</div><!-- #primary -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/single-post.js"></script>
 <?php
 get_footer();
 ?>
