<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div class="container">
	<div class="single-post-article">
		<article id="post-<?php the_ID(); ?>">
			<div class="offset-1 col-10">
				<div class="row">
					<div class="col-lg-8">
						<div class="meta">
							<?php the_category(); ?>
						</div>
						<header class="entry-header">
							<?php
							if ( is_single() ) :
							the_title( '<h5 class="entry-title">', '</h5>' );
							else :
								the_title( '<h5 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h5>' );
							endif;

							if ( 'post' === get_post_type() ) : ?>
							<!-- <div class="entry-meta">
							<?php wp_bootstrap_starter_posted_on(); ?>
						</div> -->
						<!-- .entry-meta -->
						<?php
						endif; ?>
						</header><!-- .entry-header -->
						<div class="main-content">
							<?php
							if ( is_single() ) :
								the_content();
								else :
									the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'wp-bootstrap-starter' ) );
								endif;
								?>
						</div>
						<div class="other-content">
							<p>
								<?php
								echo the_field('tresc');
								?>
							</p>
						</div>
						<?php
						the_post_navigation( array(
            'prev_text' => __( 'Poprzedni artykuł' ),
            'next_text' => __( 'Następny artykuł' ),
        		) );;
						?>
					</div>
					<div class="right-sidebar offset-lg-1 col-lg-3">
						<ul>
							<?php wp_list_categories( array(
					        'orderby' => 'name'
					    ) ); ?>
						</ul>
						<h4 class="news">aktualności</h4>
						<ul class="post-news">
							<?php
								$recent_posts = wp_get_recent_posts();
								foreach( $recent_posts as $recent ){
									echo
									'<li>
										<div class="row">
											<div class="post-news--single col-12">
												<div class="row">
													<div class="col-3 col-lg-4">
														<img src="' . get_the_post_thumbnail_url($recent["ID"]) . '">
													</div>
													<div class="col-9 col-lg-8">
														<a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a>
													</div>
												</div>
											</div>
										</div>
									</li>';
								}
								wp_reset_query();
							?>
							</ul>
					</div>
				</div>
			</div>
	</article><!-- #post-## -->
</div>
