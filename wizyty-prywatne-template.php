<?php /* Template Name: Podstrona Wizyt Prywatnych */ ?>

<?php
/**
 * Template Name: Podstrona Wizyt Prywatnych
 *
 * @package WordPress
 * @subpackage daramed
 * @since daramed 1.0


 */get_header(); ?>
	<section class="content-area col-12" id="uslugi-medyczne">

			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<?php get_sidebar(); ?>
					</div>
					<div class="col-md-9">
						<div class="row mb-2 wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12 d-flex">
								<div class="specialization-icon__nfz" style="background-image: url('<?= the_field('ikona'); ?>')">

								</div>
								<h4 class="my-0 ml-3" style="font-size:30px; line-height:1;"><?= the_field('nazwa'); ?></h4>
								<div class="d-flex align-items-end">
									<span class="primary-bg-span text--small text-uppercase" style="margin-bottom:5px; margin-left:5px;"><?= the_field('terminy');?></span>
								</div>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<p><?= the_field('opis'); ?></p>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<h5>Lekarze</h5>
							</div>
						</div>
						<?php if( have_rows('lekarz') ): ?>
						<?php while ( have_rows('lekarz') ) : the_row(); ?>
						<div class="row wow fadeIn" data-wow-delay="0.4s">
							<div class="col-md-5">
								<div class="row">
									<div class="col-12">
										<div class="doctor-profile medical-stethoscope">
											<p class="text--normal"><?= the_sub_field('lekarz_nazwa'); ?></p>
										</div>
										<div class="doctor-profile">
											<?php while ( have_rows('grupa-tagow') ) : the_row(); ?>
											<span class="primary-bg-span text--small"><?= the_sub_field('tag'); ?></span>
											<?php endwhile; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="row mb-3 mt-2">
									<?php while ( have_rows('terminy-przyjec') ) : the_row(); ?>
									<div class="col-3 col-sm-2 col-md-3 pl-md-2 admission">
										<p class="text--small text-center w-100 admission--day"><?= the_sub_field('dzien-przyjec'); ?></p>
										<p class="text--small text-center w-100 admission--time"><?= the_sub_field('godziny-przyjec'); ?></p>
									</div>
									<?php endwhile; ?>
								</div>
							</div>
						</div>
						<?php endwhile; ?>
						<?php endif; ?>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<h5>Usługi</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-md-10" id="specjalizacje">
								<div class="row">
									<?php if( have_rows('uslugi') ): ?>
									<?php while ( have_rows('uslugi') ) : the_row(); ?>
									<div class="col-md-3 m-auto specjalizacje wow fadeInUp" data-wow-delay="0.2s">
										<div class="specjalizacje__ginekologia-img specjalizacje__ginekologia-img m-auto" style="background-image: url('<?= the_sub_field('ikona'); ?>')"></div>
										<p class="text-center"><?= the_sub_field('opis'); ?></p>
									</div>
									<?php endwhile; ?>
									<?php endif; ?>
								</div>
							</div>
						</div>
						<div class="row wow fadeInUp" data-wow-delay="0.2s">
							<div class="col-12">
								<h5>Cennik</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<table class="tg wow fadeInUp" data-wow-delay="0.2s">
								  <tr class="tg wow fadeIn" data-wow-delay="0.4s">
								    <th class="tg-nazwa">Nazwa usługi</th>
								    <th class="tg-opis">Krótki opis</th>
								    <th class="tg-suma">Sumuj koszt</th>
								    <th class="tg-cena">Cena</th>
								  </tr>
									<?php while ( have_rows('cennik') ) : the_row(); ?>
								  <tr class="tg wow fadeIn" data-wow-delay="0.4s">
								    <td class="tg-nazwa"><?= the_sub_field('nazwa-uslugi'); ?></td>
								    <td class="tg-opis"><?= the_sub_field('krotki-opis'); ?></td>
										<td class="tg-suma">
											<label>
											  <input type="checkbox">
											  <span class="checkmark"></span>
											</label>
										</td>
								    <td class="tg-cena"><span><?= the_sub_field('cena'); ?></span> zł</td>
								  </tr>
									<?php endwhile; ?>
									<tr class="tg wow fadeIn" data-wow-delay="0.4s">
								    <td colspan="4" class="tg-zaplata"><span>0.00</span> zł</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section><!-- #primary -->

<?php
get_footer();
